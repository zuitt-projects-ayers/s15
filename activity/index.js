function oddEvenChecker(num){
	if(typeof num === "number"){
		if(num % 2 === 0){
			console.log("The number is even.")
		} else {
			console.log("The number is odd.")
		}
	} else {
		alert("Invalid Input.")
	}
}

oddEvenChecker(12)
oddEvenChecker(15)

function budgetChecker(budget){
	if(typeof budget === "number"){
		if(budget > 40000){
			console.log("You are over the budget.")
		} else {
			console.log("You have resources left.")
		}
	} else {
		alert("Invalid Input.")
	}
}

budgetChecker(500000)
budgetChecker(20)